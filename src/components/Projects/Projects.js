import React from "react";
import "./projects.scss";
import proj1 from "../../images/quhaad.jpg";
import proj2 from "../../images/clauction.jpg";
// import proj3 from "../../images/proj3.jpg";
// import proj4 from "../../images/proj4.jpg";
// import proj5 from "../../images/proj5.jpg";

const Projects = () => {
  return (
    <section className="c-projects">
      <div className="l-container">
        <h1 className="c-projects__heading">Projects</h1>
        <div className="l-projects__wrapper">
          <div className="c-proj">
            <figure>
              <img src={proj1} alt="placeholder1" />
              <h2>Quhaad</h2>
              <p>
                Quhaad is an interactive game website built using React and
                Spring
              </p>
            </figure>
            <button>
              <a href="https://quhaad.now.sh/">View project</a>
            </button>
          </div>
          <div className="c-proj">
            <figure>
              <img src={proj2} alt="placeholder2" />
              <h2>Clauction</h2>
              <p>Clauction is an online bidding website built using Django</p>
            </figure>
            <button>
              <a href="https://clauction.herokuapp.com">View project</a>
            </button>
          </div>
          <div className="c-proj">
            <figure>
              {/* <img src={proj4} alt="placeholder4" /> */}
              <h2>My Portfolio</h2>
              <p>Portfolio Website built using React</p>
            </figure>
            <button>
              <a href="https://gitlab.com/amaliahanisa/my-portfolio">
                View project
              </a>
            </button>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Projects;
