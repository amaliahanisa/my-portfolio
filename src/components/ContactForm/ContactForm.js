import React from "react";
import "./contact-form.scss";
import LinkedInIcon from "@material-ui/icons/LinkedIn";
import GitHubIcon from "@material-ui/icons/GitHub";
import EmailIcon from "@material-ui/icons/Email";

const Contact = () => {
  return (
    <section className="c-contact" id="contact">
      <div className="l-container">
        <h1>Contact</h1>
        <div className="contact-icon">
          <a href="https://www.linkedin.com/in/amalia-hanisafitri-4b436a180/">
            {" "}
            <LinkedInIcon style={{ fontSize: 70, color: "white" }} />
          </a>
          <a href="https://gitlab.com/amaliahanisa">
            {" "}
            <GitHubIcon style={{ fontSize: 65, color: "white" }} />
          </a>
          <a href="mailto:amaliahanisafitri@gmail.com">
            {" "}
            <EmailIcon style={{ fontSize: 65, color: "white" }} />
          </a>
        </div>
      </div>
    </section>
  );
};

export default Contact;
