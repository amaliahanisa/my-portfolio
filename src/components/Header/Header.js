import React from "react";
import "./header.scss";
import { animateScroll as scroll } from "react-scroll";
import HomeIcon from "@material-ui/icons/Home";

const Header = () => {
  return (
    <header className="c-header">
      <HomeIcon
        style={{ fontSize: 50, color: "white" }}
        className="c-header__logo"
        onClick={() => scroll.scrollToTop()}
      />
    </header>
  );
};

export default Header;
