import React from "react";
import "./main.scss";
import photo from "../../images/profile.jpg";
// import arrowDown from "../../images/arrow-down.svg";
import { Link } from "react-scroll";
import Particles from "react-tsparticles";

const Main = () => {
  return (
    // Complete React Code
    <div className="main">
      <div className="main-content">
        <img src={photo}></img>
        <h1>Hello.</h1>
        <h1>I am Amalia</h1>
        <p>Feel free to take a look at my latest projects</p>
        <Link className="c-main__contact" to="contact" smooth duration={1000}>
          Contact
        </Link>
        <Link
          className="c-main__projects"
          to="c-projects"
          smooth
          duration={1000}
        >
          Projects
        </Link>
      </div>
      <Particles
        id="tsparticles"
        options={{
          background: {
            color: {
              value: "415a77",
            },
          },
          fpsLimit: 60,
          interactivity: {
            detectsOn: "canvas",
            events: {
              onClick: {
                enable: true,
                mode: "push",
              },
              onHover: {
                enable: true,
                mode: "repulse",
              },
              resize: true,
            },
            modes: {
              bubble: {
                distance: 400,
                duration: 2,
                opacity: 0.8,
                size: 40,
              },
              push: {
                quantity: 4,
              },
              repulse: {
                distance: 200,
                duration: 0.4,
              },
            },
          },
          particles: {
            color: {
              value: "#ffffff",
            },
            links: {
              color: "#ffffff",
              distance: 150,
              enable: true,
              opacity: 0.5,
              width: 1,
            },
            collisions: {
              enable: true,
            },
            move: {
              direction: "none",
              enable: true,
              outMode: "bounce",
              random: false,
              speed: 6,
              straight: false,
            },
            number: {
              density: {
                enable: true,
                value_area: 800,
              },
              value: 80,
            },
            opacity: {
              value: 0.5,
            },
            shape: {
              type: "circle",
            },
            size: {
              random: true,
              value: 5,
            },
          },
          detectRetina: true,
        }}
      />
    </div>
  );
};

export default Main;
